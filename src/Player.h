#ifndef PLAYER_H_
#define PLAYER_H_

/* Ogre3d Graphics*/
#include "Ogre.h"

/* Bullet3 Physics */
#include "btBulletDynamicsCommon.h"
#include "btBulletCollisionCommon.h"

using namespace Ogre;

/** Example player class.
* Written to illistrate the connection of Ogre/Bullet.
* Essentially just a wrapper around the cube object setup code.
*/
class Player
{
private:
  SceneNode* boxSceneNode;     /**< Scene graph node */
  Entity* box;                 /**< Mesh entity */
  Vector3 meshBoundingBox;     /**< Size of the bounding mesh from ogre */

  btCollisionShape* colShape;  /**< Collision shape, describes the collision boundary */
  btRigidBody* body;           /**< Rigid Body */
  btDiscreteDynamicsWorld* dynamicsWorld;  /**< physics/collision world */

  int Height = 6;


public:
  Player();
  ~Player();


  bool leftRotation;
  bool rightRotation;
  /**
  * Creates the mesh.
  * @param scnMgr the Ogre SceneManager.
  */
  void createMesh(SceneManager* scnMgr);

  SceneNode*  getSceneNode();

  /**
  * Creates a new child of the given parent node, adds the mesh to it.
  * @param parent, the parent (in the scene graph) of the node the player will be attatched to.
  */
  void attachToNode(SceneNode* parent);

  double getRadian();

  /**
  * Sets the scale.
  * @param x, scale on the x axis.
  * @param y, scale on the y axis.
  * @param z, scale on the z axis.
  */
  void setScale(float x, float y, float z);
  Vector3 getScale();
  /**
  * Sets the orientation.
  * @param axis, vector about which the orientation takes place.
  * @param angle, angle (in radians).
  */
  void setRotation(Vector3 axis, Radian angle);
  Quaternion getRotation();
 // (Vector3 + Radian) getRotation(Vector3 axis, Radian rads);
  /**
  * Sets the position.
  * @param x, position on the x axis.
  * @param y, position on the y axis.
  * @param z, position on the z axis.
  */  void setPosition(float x, float y, float z);
      //void getPosition(float x, float y, float z);
		Vector3 getPosition();


	  void setHeight(int h);
	  int getHeight() const { return Height; }


  /**
  * Fudge to get the bouning box from Ogre3d, at a it might work for other shapes.
  */
  void boundingBoxFromOgre();

  /**
  * Creates a new ridgid body of the given mass.
  * @param mass
  */
  void createRigidBody(float mass);
  /**
  * Add this collision shape to the collision shapes list
  * @param collisionShaps, the list of collision shapes (shared with the physics world).
  */
  void addToCollisionShapes(btAlignedObjectArray<btCollisionShape*> &collisionShapes);
  /**
  *  Add this rigid body to the dynamicsWorld.
  * @param dynamicsWorld, the wrold we're going to add ourselves to.
  */
  void addToDynamicsWorld(btDiscreteDynamicsWorld* dynamicsWorld);

  /**
  * What on Earth is this for!? Can't change the mass of a rigid body.
  * @param mass
  */
  void setMass(float mass);

  /**
  * Update, um ... makes coffee.
  */
  void update();

  void forwardForce();
  void boostForce();
  void liftForce();
  void constForwardForce();
  void leftRoation();
  void rightRoation();
  void slowDown();
};


#endif
