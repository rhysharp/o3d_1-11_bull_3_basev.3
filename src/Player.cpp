#include "Player.h"
#//33333333/////

Player::Player()
{
  boxSceneNode = nullptr;
  box = nullptr;
  Vector3 meshBoundingBox(0.0f,0.0f,0.0f);

  colShape = nullptr;
  dynamicsWorld = nullptr;
}

Player::~Player()
{

}

void Player::setHeight(int h)
{
}

void Player::createMesh(SceneManager* scnMgr)
{
  box = scnMgr->createEntity("shipBareBones.mesh");
}

void Player::attachToNode(SceneNode* parent)
{
  boxSceneNode = parent->createChildSceneNode();
  boxSceneNode->attachObject(box);
  boxSceneNode->setScale(70.0f, 70.0f, 70.0f);
  boundingBoxFromOgre();

  
}

double Player::getRadian()
{
	return double();
}

//SCALE
void Player::setScale(float x, float y, float z)
{
    boxSceneNode->setScale(x,y,z);
}
Vector3 Player::getScale()
{
	return boxSceneNode->getScale();
}

//ROTATION
void Player::setRotation(Vector3 axis, Radian rads)
{
  //quat from axis angle
  Quaternion quat(rads, axis);
  boxSceneNode->setOrientation(quat);
}

Quaternion Player::getRotation()
{
	return Quaternion();
}


//Vector4 Player::getRotation(Vector3 axis, Radian rads)
//{
//	return Vector4();
//}

//(btQuaternion(quat2.x, quat2.y, quat2.z, quat2.w));

//float Player::getRadian() 
//{
//	return boxSceneNode->getRadian();
//}

//POSITION
void Player::setPosition(float x, float y, float z)
{
  boxSceneNode->setPosition(x,y,z);
}
Vector3 Player::getPosition()
{
	return boxSceneNode->getPosition();
}


void Player::boundingBoxFromOgre()
{
  //get bounding box here.
  boxSceneNode->_updateBounds();
  const AxisAlignedBox& b = boxSceneNode->_getWorldAABB();
  Vector3 temp(b.getSize());
  meshBoundingBox = temp;
  boxSceneNode->showBoundingBox(false);
}

void Player::createRigidBody(float bodyMass)
{
  colShape = new btBoxShape(btVector3(meshBoundingBox.x/2.0f, meshBoundingBox.y/2.0f, meshBoundingBox.z/2.0f));

  /// Create Dynamic Objects
  btTransform startTransform;
  startTransform.setIdentity();

  Quaternion quat2 = boxSceneNode->_getDerivedOrientation();
  startTransform.setRotation(btQuaternion(quat2.x, quat2.y, quat2.z, quat2.w));

  Vector3 pos = boxSceneNode->_getDerivedPosition();
  startTransform.setOrigin(btVector3(pos.x, pos.y, pos.z));

  btScalar mass(bodyMass);

  //rigidbody is dynamic if and only if mass is non zero, otherwise static
  bool isDynamic = (mass != 0.f);

  btVector3 localInertia(0, 0, 0);
  if (isDynamic)
  {
      // Debugging
      //std::cout << "I see the cube is dynamic" << std::endl;
      colShape->calculateLocalInertia(mass, localInertia);
  }

  //using motionstate is recommended, it provides interpolation capabilities, and only synchronizes 'active' objects
  btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
  btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, colShape, localInertia);
  body = new btRigidBody(rbInfo);

  //Set the user pointer to this object.
  body->setUserPointer((void*)this);
}

void Player::addToCollisionShapes(btAlignedObjectArray<btCollisionShape*> &collisionShapes)
{
  collisionShapes.push_back(colShape);
}


void Player::addToDynamicsWorld(btDiscreteDynamicsWorld* dynamicsWorld)
{
  this->dynamicsWorld = dynamicsWorld;
  dynamicsWorld->addRigidBody(body);
}

void Player::update()
{
  btTransform trans;

  Player::constForwardForce();

  if (body && body->getMotionState())
  {
    body->getMotionState()->getWorldTransform(trans);
    btQuaternion orientation = trans.getRotation();

	Vector3 offset = Ogre::Vector3(0.0f, meshBoundingBox.y, 0.0f);
	Vector3 bob = Ogre::Vector3(trans.getOrigin().getX(), trans.getOrigin().getY(), trans.getOrigin().getZ());

	boxSceneNode->setPosition(bob);
    boxSceneNode->setOrientation(Ogre::Quaternion(orientation.getW(), orientation.getX(), orientation.getY(), orientation.getZ()));

	offset = boxSceneNode->getOrientation() * offset;
	boxSceneNode->setPosition(boxSceneNode->getPosition() - offset);

  }

}

SceneNode*  Player::getSceneNode()
{
	return this->boxSceneNode;
}

void Player::constForwardForce()
{
	btTransform trans;
	body->getMotionState()->getWorldTransform(trans);
	body->setFriction(2);
	btVector3 force(5.0f, 0.0f, 0.0f); //20

	btQuaternion orientation = trans.getRotation();

	btVector3 push;

	push = quatRotate(orientation, force);			//need to add a friction affect somewhere, slowing down the player
	body->activate(true);
	body->applyCentralForce(push);
}

void Player::boostForce()
{
	btTransform trans;
	body->getMotionState()->getWorldTransform(trans);
	body->setFriction(2);
	btVector3 force(5040.0f, 0.01f, 0.0f);
	btQuaternion orientation = trans.getRotation();

	btVector3 push;

	push = quatRotate(orientation, force);			//need to add a friction affect somewhere, slowing down the player
	body->activate(true);
	body->applyCentralForce(push);
	//body->clearForces();
}

void Player::forwardForce()
{
	btTransform trans;
	body->getMotionState()->getWorldTransform(trans);
	body->setFriction(2);
	btVector3 force(140.0f, 0.01f, 0.0f);
	btQuaternion orientation = trans.getRotation();

	btVector3 push;
	
	push = quatRotate(orientation, force);			//need to add a friction affect somewhere, slowing down the player
	body->activate(true);
	body->applyCentralForce(push);
	//body->clearForces();
	}

void Player::leftRoation()
{
	btTransform trans;
	body->getMotionState()->getWorldTransform(trans);
	body->setFriction(4);
	
	btVector3 rForce(0.0f, 110.0f, 0.0f);
	
	btQuaternion orientation = trans.getRotation();
		
	btVector3 rotate;

	//need to add a friction affect somewhere, slowing down the player
	rotate = quatRotate(orientation, rForce);
	body->activate(true);
	//body->applyCentralForce(push); 
	body->applyTorqueImpulse(rotate);
	body->clearForces();
	}

void Player::rightRoation()
{
	btTransform trans;
	body->getMotionState()->getWorldTransform(trans);
	body->setFriction(4);

	btVector3 rForce(0.0f, -110.0f, 0.0f);

	btQuaternion orientation = trans.getRotation();
	
	btVector3 rotate;

	//need to add a friction affect somewhere, slowing down the player
	rotate = quatRotate(orientation, rForce);
	body->activate(true);
	//body->applyCentralForce(push); 
	body->applyTorqueImpulse(rotate);
	body->clearForces();
}
 
void Player::liftForce()
{
	btTransform trans;
	body->getMotionState()->getWorldTransform(trans);
	body->setFriction(2);
	btVector3 force(0.0f, 40.0f, 0.0f);

	btQuaternion orientation = trans.getRotation();

	btVector3 push;
	body->activate(true);
	push = quatRotate(orientation, force);			//need to add a friction affect somewhere, slowing down the player

	body->applyCentralForce(push);
}







													//Need to add a rotational effect while maintaining the forward momentum.  

//void Player::leftRoation()
//{
//	btTransform trans;
//	body->getMotionState()->getWorldTransform(trans);
//
//	btVector3 force(10.0f, 0.0f, 0.0f);	//forward force, not rotational
//
//	btQuaternion orientation = trans.getRotation();
//
//	btVector3 push;
//
//	push = quatRotate(orientation, force);			//need to add a friction affect somewhere, slowing down the player
//	left = force.rotate()
//	
//	body->applyCentralForce(push);
//
//}